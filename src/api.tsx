import parseLinkHeader from 'parse-link-header';

const apiUrl = 'https://www.anapioficeandfire.com/api';

/**
 * https://www.anapioficeandfire.com/Documentation#pagination
 * Params:
 * - page (default 1)
 * - pageSize (default 10, max 50)
 */
const fetchCharacters = (page: number, pageSize: number) => {
  return fetch(`${apiUrl}/characters?page=${page}&pageSize=${pageSize}`)
    .then(res => {
      const link = parseLinkHeader(res.headers.get('link') || '');
      
      return res.json().then(parsedJson => {
        if (parsedJson.length < 1) throw Error('no results');
        return {
          characters: parsedJson,
          lastPage: link ? link.last : 1,
        }
      });
    })
    .catch((err) => err);
}

export {
  fetchCharacters,
}