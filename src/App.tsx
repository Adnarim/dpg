import { useState, useEffect } from 'react';
import { Alert } from 'react-bootstrap';

import { fetchCharacters } from './api';
import ListCharacters from './components/listCharacters';
import PaginationBar from './components/pagination';

import './App.scss';

function App() {
  const [characters, setCharacters] = useState([]);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(100);
  const [perPage/*, setPerPage*/] = useState(12);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    setIsLoading(true);

    const fetchData = () => {
      fetchCharacters(page, perPage)
        .then(result => {
          if (!result.characters) {
            setError('No results');
            setIsLoading(false);
          } else {
            setCharacters(result.characters);
            setTotal(result.lastPage.page);
            setIsLoading(false);
            window.scroll(0,0);
          }
        })
        .catch(err => {
          setError(err);
          setIsLoading(false);
        });
    }

    fetchData();
  }, [page, perPage]);


  const navigateTo = (toPage: number) => {
    setPage(toPage);
  }

  return (
    <div className="App">
      <div className="header"></div>

      <div className="container rounded">
        <h1>Characters</h1>

        {isLoading &&
          <div className="loading-overlay">
            <div className="loading-inner shadow-sm rounded">
              Loading...
            </div>
          </div>
        }

        {error && <Alert variant="error">{error}</Alert>}
        
        {characters && <ListCharacters characters={characters} />}
        <PaginationBar page={page} total={total} navigateTo={(toPage: number) => navigateTo(toPage)} />
      </div>
    </div>
  );
}

export default App;
