import { Pagination } from 'react-bootstrap';

import './pagination.scss'

type Props = {
  page: number,
  total: number,
  navigateTo: (arg0: number) => void,
}

const PaginationBar = ({page, total, navigateTo}: Props) => {
  let active = page;
  let items = [];

  for (let number = 1; number <= total; number++) {
    items.push(
      <Pagination.Item
        key={number}
        active={number === active}
        onClick={() => navigateTo(number)}
      >
        {number}
      </Pagination.Item>
    );
  }
  
  return <Pagination className="d-flex flex-wrap">
    <Pagination.First onClick={() => navigateTo(1)} />
    {items}
    <Pagination.Last onClick={() => navigateTo(total)} />
  </Pagination>
};

export default PaginationBar;