import React from 'react';
import renderer from 'react-test-renderer';
import PaginationBar from './index';

test('PaginationBar matches snapshot', () => {
  const component = renderer.create(
    <PaginationBar page={1} total={5} navigateTo={() => {}} />,
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
