import React from 'react';
import renderer from 'react-test-renderer';
import ListCharacters from './index';

const characters = [
  {
    "url": "https://anapioficeandfire.com/api/characters/583",
    "name": "Jon Snow",
    "gender": "Male",
    "culture": "Northmen",
    "born": "In 283 AC",
    "died": "",
    "titles": [
      "Lord Commander of the Night's Watch"
    ],
    "aliases": [
      "Lord Snow",
      "Ned Stark's Bastard",
      "The Snow of Winterfell",
      "The Crow-Come-Over",
      "The 998th Lord Commander of the Night's Watch",
      "The Bastard of Winterfell",
      "The Black Bastard of the Wall",
      "Lord Crow"
    ],
    "father": "",
    "mother": "",
    "spouse": "",
    "allegiances": [
      "https://anapioficeandfire.com/api/houses/362"
    ],
    "books": [
      "https://anapioficeandfire.com/api/books/5"
    ],
    "povBooks": [
      "https://anapioficeandfire.com/api/books/1",
      "https://anapioficeandfire.com/api/books/2",
      "https://anapioficeandfire.com/api/books/3",
      "https://anapioficeandfire.com/api/books/8"
    ],
    "tvSeries": [
      "Season 1",
      "Season 2",
      "Season 3",
      "Season 4",
      "Season 5",
      "Season 6"
    ],
    "playedBy": [
      "Kit Harington"
    ]
  }
];

describe('ListCharacters component', () => {
  test('matches snapshot', () => {
    const component = renderer.create(
      <ListCharacters characters={characters} />,
    );
  
    let tree = component.toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree).toMatchSnapshot();
  });
  
  test('renders correctly without characters', () => {
    const component = renderer.create(
      <ListCharacters characters={[]} />,
    );
  
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
})
