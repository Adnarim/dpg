import { Card } from 'react-bootstrap';

interface Character {
  url: string,
  name: string,
  aliases: string[],
  gender: string,
  culture: string,
  playedBy: string[],
  tvSeries: string[],
};

type Characters = Character[];

type Props = {
  characters: Characters,
};

const ListCharacters = ({characters}: Props) => {
  return <div className="d-grid results-list">
    {characters.map((character: Character, key: number) => {
      return <Card key={key} className="mb-3 result-item">
        <Card.Header>
          {character.aliases.join() ? character.aliases.join(', ') : character.name ? character.name : 'unknown'}
        </Card.Header>

        <Card.Body>
          <Card.Title>Name: {character.name || 'unknown'}</Card.Title>
          <div className="d-flex flex-column">
            <div className="data-row">
              <div className="label">Gender</div>
              <div className="value">{character.gender || 'unknown'}</div>
            </div>

            <div className="data-row">
              <div className="label">Culture</div>
              <div className="value">{character.culture || 'unknown'}</div>
            </div>

            <div className="data-row">
              <div className="label">Played by</div>
              <div className="value">{character.playedBy || 'unknown'}</div>
            </div>
          </div>
        </Card.Body>
      </Card>
    })}
  </div>;
}

export default ListCharacters;